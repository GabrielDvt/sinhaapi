<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transferencia;
use App\Estoque;

class TransferenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Transferencia::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $transferencia = Transferencia::create($request->all());
        return response()->json($transferencia);
    }


    /**
     * Transfere o estoque de uma loja para outra, registra essa transferência, remove a quantidade do estoque anterior
     * e cria um novo estoque com essa quantidade
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function transferir(Request $request)
    {   
        dd($request->all());
        $idEstoqueOrigem = $request->input('idEstoqueOrigem');
        $quantidade = $request->input('quantidade');
        $idLoja = $request->input('idLoja');

        //busca o estoque de destino
        $estoque = Estoque::find($idEstoqueOrigem);
        $transferencia = new Transferencia();
        $transferencia->estoque_origem = $estoque->id;
        $transferencia->funcionario_id = $estoque->funcionario_id;

        //remove do estoque anterior
        $estoque->quantidade -= $quantidade;

        //cria um novo estoque
        $estoqueDestino = new Estoque();
        $estoqueDestino->unidade_id = $estoque->unidade_id;
        $estoqueDestino->produto_id = $estoque->produto_id;
        $estoqueDestino->loja_id = $idLoja;
        $estoqueDestino->quantidade = $quantidade;
        $estoqueDestino->save();

        //relaciona a transferência com o novo estoque
        $transferencia->estoqueDestino = $estoqueDestino->id;

        //salva a transferência
        $transferencia->save();

        return response()->json($transferencia, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transferencia = Transferencia::find($id);
        return response()->json($transferencia);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
