<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estoque;
use DB;

class EstoquesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Estoque::all();
    }

    /* dado o id da loja, busca o estoque */
    public function buscarPorLoja($id) 
    {
        $estoque = Estoque::where('loja_id', $id)
            ->groupBy('estoque.produto_id', 'estoque.unidade_id')
            ->leftJoin('produtos', 'produtos.id', '=', 'estoque.produto_id')
            ->leftJoin('unidades', 'unidades.id', '=', 'estoque.unidade_id')
            ->leftJoin('lojas', 'lojas.id', '=', 'estoque.loja_id')
            ->select('estoque.*', 'produtos.nome AS nome', 'estoque.quantidade AS quantidade', 'lojas.nome AS loja', 'unidades.unidade AS unidade', DB::raw('SUM(estoque.quantidade) AS quantidade'))
            ->get();
        return response()->json($estoque);
    }

    /* dado o id do primeiro estoque, remover a quantidade do primeiro estoque e transferir  */
    public function transferir($idEstoque1, $idEstoque2)
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $estoque = Estoque::create($request->all());
        return response()->json($estoque, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $estoque = Estoque::find($id);
        return response()->json($estoque);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estoque = Estoque::find($id);
        $estoque->update($request->all());

        return response()->json($estoque, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estoque = Estoque::find($id);
        $estoque->delete();

        return response()->json(null, 204);
    }
}
