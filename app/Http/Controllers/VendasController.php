<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venda;
use DB;
use Carbon\Carbon;

class VendasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return Venda::all();
        return DB::table('vendas')
            ->leftJoin('clientes', 'vendas.cliente_id', '=', 'clientes.id')
            ->leftJoin('produtos', 'vendas.produto_id', '=', 'produtos.id')
            ->select('produtos.nome AS nomeProduto', 'clientes.nome AS nomeCliente', 'produtos.*', 'clientes.*', 'vendas.*')
            ->orderBy('vendas.id', 'DESC')
            ->get();
    }

    /**
    * A cada produto vendido, buscar a soma da quantidade desses produtos
    */
    public function buscarQtdePorProduto() 
    {
        return DB::table('vendas')
            ->groupBy('produto_id')
            ->leftJoin('produtos', 'vendas.produto_id', '=', 'produtos.id')
            ->select(DB::raw('nome, SUM(quantidade) AS quantidade'))
            ->get();
    }

    public function buscarQtdePorProdutoComIntervalo($dtInicial, $dtFinal) 
    {

        $dtInicial = Carbon::parse($dtInicial);
        $dtFinal = Carbon::parse($dtFinal);
        
        return DB::table('vendas')
            ->groupBy('produto_id')
            ->whereBetween('vendas.created_at', [$dtInicial, $dtFinal->endOfDay()])
           /* ->where('vendas.created_at', '>=', $dtInicial)
            ->where('vendas.created_at', '<', $dtFinal)*/
            ->leftJoin('produtos', 'vendas.produto_id', '=', 'produtos.id')
            ->select(DB::raw('nome, SUM(quantidade) AS quantidade'))
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $venda = Venda::create($request->all());
        return response()->json($venda, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venda = Venda::find($id)
              ->leftJoin('clientes', 'vendas.cliente_id', '=', 'clientes.id')
              ->leftJoin('produtos', 'vendas.produto_id', '=', 'produtos.id')
              ->select('produtos.nome AS nomeProduto', 'clientes.nome AS nomeCliente', 'produtos.*', 'clientes.*', 'vendas.*')
              ->get();
        return response()->json($venda);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $venda = Venda::find($id);
        $venda->update($request->all());

        return response()->json($venda, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $venda = Venda::find($id);
        $venda->delete();

        return response()->json(null, 204);
    }
}
