<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('produtos', 'ProdutosController', ['except' => ['create', 'edit']]);
Route::resource('clientes', 'ClientesController',  ['except' => ['create', 'edit']]);
Route::resource('funcionarios', 'FuncionariosController',  ['except' => ['create', 'edit']]);
Route::resource('vendas', 'VendasController',  ['except' => ['create', 'edit']]);
Route::resource('lojas', 'LojasController', ['except' => ['create', 'edit']]);
Route::resource('unidades', 'UnidadesController', ['except' => ['create', 'edit']]);
Route::resource('estoques', 'EstoquesController', ['except' => ['create', 'edit']]);
Route::resource('transferencias', 'TransferenciasController', ['except' => ['create', 'edit']]);


Route::get('buscarQtdePorProduto', 'VendasController@buscarQtdePorProduto');
Route::get('buscarQtdePorProduto/{dtInicial}/{dtFinal}', 'VendasController@buscarQtdePorProdutoComIntervalo');

/* rotas para o estoque */
//buscar os produtos que tem em estoque em uma determinada loja
Route::get('buscarEstoque/{idLoja}', 'EstoquesController@buscarPorLoja');

Route::post('transferir', 'TransferenciasController@transferir');

