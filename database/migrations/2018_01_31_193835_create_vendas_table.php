<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id')->unsigned();
            $table->integer('produto_id')->unsigned();
            $table->integer('funcionario_id')->unsigned()->nullable();
            $table->integer('quantidade');
            $table->float('desconto')->nullable();
            $table->string('pagamento')->nullable();
            $table->float('total')->nullable();
            $table->date('data')->nullable();
            $table->float('preco_unidade')->nullable();

            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->foreign('produto_id')->references('id')->on('produtos');
            $table->foreign('funcionario_id')->references('id')->on('funcionarios');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendas');
    }
}
