<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstoqueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estoque', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidade_id')->unsigned();
            $table->integer('produto_id')->unsigned();
            $table->integer('loja_id')->unsigned();
            $table->integer('quantidade');

            $table->foreign('unidade_id')->references('id')->on('unidades');
            $table->foreign('produto_id')->references('id')->on('produtos');
            $table->foreign('loja_id')->references('id')->on('lojas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estoque');
    }
}
