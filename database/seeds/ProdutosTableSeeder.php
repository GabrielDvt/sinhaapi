<?php

use Illuminate\Database\Seeder;

class ProdutosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos')->insert(
        [
           'nome' => 'Short 2 Pele',
           'preco_custo' => 4.00,
           'preco_venda' => 5.00,
       ],
       [
           'nome' => 'Regata c/ Bojo',
           'preco_custo' => 4.00,
           'preco_venda' => 5.00,
       ],
       [
           'nome' => 'Blusa',
           'preco_custo' => 4.00,
           'preco_venda' => 7.50,
       ],
       [
           'nome' => 'Bermuda',
           'preco_custo' => 4.00,
           'preco_venda' => 7.00,
       ],
       [
           'nome' => 'Top',
           'preco_custo' => 4.00,
           'preco_venda' => 10.00,
       ]
       );
    }
}
