<?php

use Illuminate\Database\Seeder;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert(
        [
           'nome' => 'Anônimo',
           'email' => 'email@email.com',
           'telefone' => '(62) 9100-0000'
       ]);
    }
}
