<?php

use Illuminate\Database\Seeder;

class VendasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendas')->insert(
        [
           "cliente_id"=> 1,
			"produto_id"=> 1,
			"quantidade"=> 1,
			"pagamento"=> "cheque",
			"preco_unidade"=> 50,
			"total" => 50,
			"funcionario_id"=> 1
       ]);
    }
}
