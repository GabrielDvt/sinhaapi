<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ClientesTableSeeder::class);
        $this->call(FuncionariosTableSeeder::class);
        $this->call(ProdutosTableSeeder::class);
        $this->call(VendasTableSeeder::class);
        $this->call(UnidadesTableSeeder::class);
       // $this->call(EstoqueTableSeeder::class);
    }
}
